/*
 * Copyright (c) 2019, Liberty Mutual Group
 *
 * Created on Aug 27, 2019
 */
package com.libertymutual.student.spencer.programs.example01.shapes;

import static org.junit.Assert.assertTrue;

import org.junit.Rule;
import org.junit.Test;
import org.junit.contrib.java.lang.system.ExpectedSystemExit;
import com.libertymutual.student.spencer.programs.example01.*;

public class AppTest {

  @Rule
  public final ExpectedSystemExit exit = ExpectedSystemExit.none();
  /**
   * Rigorous Test :-)
   */
  @Test
  public void shouldAnswerWithTrue()
  {
    assertTrue( true );
  }

  @Test
  public void testMain() {
    //App app = new App();
    App.main(new String[0]);
  }

}
