/*
 * Copyright (c) 2019, Liberty Mutual Group
 *
 * Created on Aug 27, 2019
 */
package com.libertymutual.student.spencer.programs.example01.shapes;

import org.junit.Test;
import org.junit.Before;

import java.awt.Color;
import java.math.BigDecimal;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class SquareTest {

  Square square;

  @Before
  public void setup(){
    square = new Square(10, Color.red);
  }

  @Test
  public void testGetArea() {
    Square square = new Square(10, Color.red);
    BigDecimal area = square.getArea();
    BigDecimal expectedAnswer = new BigDecimal(100);
    assertEquals("Verify that the area is correct:", expectedAnswer, area);
  }

  @Test
  public void testColor(){
    Square square = new Square(10, Color.red);
    assertEquals("Verify the colors", Color.red, square.getColor());
  }

  @Test
  public void thisIsATest(){
    assertTrue(true);
  }



}
