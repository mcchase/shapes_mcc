/*
 * Copyright (c) 2019, Liberty Mutual Group
 *
 * Created on Aug 27, 2019
 */
package com.libertymutual.student.spencer.programs.example01.shapes;

import org.junit.Test;
import org.junit.Before;

import java.awt.Color;
import java.math.BigDecimal;

import static org.junit.Assert.assertEquals;

public class RectangleTest {

  Rectangle rectangle;

  @Before
  public void init() {
    rectangle = new Rectangle(10, 8, Color.BLACK);
  }

  @Test
  public void testGetArea(){
    BigDecimal area = rectangle.getArea();
    BigDecimal expectedArea = new BigDecimal(80);
    assertEquals("Verify that the area is correct: ", expectedArea, area);
  }

  @Test
  public void testGetLength(){
    int length = rectangle.getLength();

    assertEquals("Assert the length", 10, length);
  }
  @Test
  public void testGetWidth(){
    int width = rectangle.getWidth();

    assertEquals("Assert the length", 8, width);
  }

}
