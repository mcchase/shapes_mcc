/*
 * Copyright (c) 2019, Liberty Mutual Group
 *
 * Created on Aug 27, 2019
 */
package com.libertymutual.student.spencer.programs.example01.shapes;
import java.awt.Color;
import java.math.BigDecimal;
// import org.apache.logging.log4j.LogManager;
// import org.apache.logging.log4j.Logger;

public class Rectangle extends Shape {
  private int length;
  private int width;

  public Rectangle(int length, int width, Color color){
    super(color);
    this.length = length;
    this.width = width;
  }

  @Override
  public BigDecimal getArea() {
    long area = length * width;
    return new BigDecimal(area);
  }

  public int getLength() {
    return length;
  }

  public int getWidth() {
    return width;
  }
}
